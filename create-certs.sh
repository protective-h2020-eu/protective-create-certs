#!/bin/bash

#   Use this script to set up certs for Warden Server or RA.

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -eo pipefail

display_help() {
    echo "Usage: $(basename "$0") KeyDir"
    echo "       KeyDir           The path to a directory where the keys and certs will be stored"
    echo "                        WARNING: This directory will be wiped first"
    echo "Environment Variables used:"
    echo "       CA_SUBJECT       The DNS name or IP of the server that is visible to the outside world"
    echo "       SSL_SUBJECT      The DNS name or IP of the server that is visible to the outside world"
    exit 1
}

# Script's args.
KEY_DIR="${1}"

if [ $# -lt 1 ]
then
    display_help
fi

# Clean the keys directory
cd "$KEY_DIR"
KEY_DIR=`pwd` # get long name
rm -rf *

cd "/var/prot_ca/"

# Generate root key
openssl genrsa -out private/ca.key.pem 4096 \
&& chmod 777 private/ca.key.pem \
&& cp private/ca.key.pem $KEY_DIR/ca.key.pem

# Generate root certificate
openssl req -new -x509 -sha256 -extensions v3_ca -days 7300 -key private/ca.key.pem -out certs/ca.cert.pem -subj "/CN=${CA_SUBJECT}" -config /var/prot_ca/openssl.cnf \
&& chmod 444 certs/ca.cert.pem \
&& cp certs/ca.cert.pem $KEY_DIR/ca.cert.pem

# Give permision to csr folder
chmod a+w -R /var/prot_ca/

cd "$KEY_DIR"

# Generate private key (server.key)
openssl genrsa -out server.key 1024

# Generate self-signed server.crt
openssl req -new -key server.key -out server.csr -subj "/CN=${SSL_SUBJECT}"
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

# # Generate root key
# openssl genrsa -out ca.key.pem 4096

# # Generate root certificate
# openssl req -new -x509 -sha256 -extensions v3_ca -days 7300 -key ca.key.pem -out ca.cert.pem -subj "/CN=${CA_SUBJECT}"
# #openssl req -new -x509 -nodes -days 7300 -key ca.key.pem -out ca.cert.pem -subj "/CN=${CA_SUBJECT}"
