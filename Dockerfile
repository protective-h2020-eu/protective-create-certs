FROM alpine

RUN apk --update add bash openssl nano

# Begin Antonio #

# #################
# Install PROT CA
RUN mkdir /var/prot_ca
WORKDIR /var/prot_ca/
RUN mkdir certs crl newcerts private clients csr
RUN chmod 700 private
RUN touch index.txt
RUN echo 1024 > serial
RUN find /var/prot_ca -type d | xargs chmod g+s
COPY ./openssl.cnf /var/prot_ca/openssl.cnf

# Library needed for prot_ca
#RUN pip install -I suds

# # Generate root key
# RUN openssl genrsa -out private/ca.key.pem 4096 \
# && chmod 777 private/ca.key.pem

# # Generate root certificate
# RUN openssl req -new -x509 -sha256 -extensions v3_ca -days 7300 -key private/ca.key.pem -out certs/ca.cert.pem -subj "/C=ES/ST=Spain/L=Barcelona/O=GMV/OU=Development/CN=10.255.0.8" \
# && chmod 444 certs/ca.cert.pem \
# && cp certs/ca.cert.pem /warden_server_keys/ca.cert.pem

# Give permision to csr folder
RUN chmod a+w -R /var/prot_ca/

# End Antonio

WORKDIR /certs

COPY create-certs.sh /usr/local/bin/create-certs.sh

CMD /usr/local/bin/create-certs.sh /certs
#CMD tail -f /dev/null

VOLUME /certs
