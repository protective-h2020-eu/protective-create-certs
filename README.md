The purpose of this repository it to generate the initial certificates (server.crt, key.pem, cert.pem) of Warden Server and Warden_ra dockers during the initial installation of the node. 

This allows to establish a connection secured by certificates between all the protective nodes that are exchanging Threat Intelligence.