#!/bin/bash

set -ex

IMAGE_NAME="protective-h2020-eu/protective-create-certs"
TAG="${1}"

REGISTRY="registry.gitlab.com"

docker push ${REGISTRY}/${IMAGE_NAME}